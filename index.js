// index.js
const express = require('express')
const path = require('path')
const bodyParser = require('body-parser')
const multer = require('multer')
const helpers = require('./helpers')
const { Car } = require('./models')
const car = require('./models/car')
const res = require('express/lib/response')
PUBLIC_DIRECTORY = path.join(__dirname, 'public')
const app = express()

app.use(express.static(PUBLIC_DIRECTORY));
app.set('view engine', 'ejs')
app.use(bodyParser.urlencoded({extended: true}))
app.use(bodyParser.json());

const storage = multer.diskStorage({
    destination: function(req, file, cb){
        cb(null, 'public/images')
    },
    filename: function(req, file, cb){
        console.log(file);
        cb(null, file.originalname)
    }
})

// GET all cars
app.get('/cars', (req, res) => {
    // const{filter, all, small, medium, large} = req.query
 Car.findAll()
   .then(cars => {
     res.json(cars)
   })
})

// GET cars by ID
app.get("/cars/:id", (req, res) => {
    const{id} = req.params;
    Car.findAll({
        where:{
            id: req.params.id
        }
    }).then(cars=>{
        res.json(cars)
    })
})

// Create cars DATA
app.post("/cars", (req, res) => {
    let upload = multer({ storage:storage, fileFilter: helpers.imageFilter }).single('image');
    upload(req, res, function(err){
        console.log(req.file);
        Car.create({
            nama: req.body.nama,
            sewa: req.body.sewa,
            ukuran: req.body.ukuran,
            image: req.file ? req.file.originalname : '',
        })
        res.json("Create Data Berhasil")
    })
})

// Update Cars Data
app.put("/cars/:id", (req, res) => {
    let upload = multer({storage:storage, fileFilter:helpers.imageFilter}).single('image')
    const{id} = req.params;
    upload(req, res, function(err){
        const cari = Car.findOne({where: {id:id}});
        Car.update({
            nama: req.body.nama,
            sewa: req.body.sewa,
            ukuran: req.body.ukuran,
            image: req.file ? req.file.originalname : cari.image,
        }, {
            where: {id:id}
        }).then(car => {
          res.json(car)
        }).catch(err => {
            res.status(422).json("Can't Update Cars")
        })

    })
})

// delete cars Data
app.delete("/cars/:id", (req, res) => {
    const{id} = req.params;
    Car.destroy({
        where:{
            id: id
        }
    }).then(() => console.log(`Car Data Deleted`))
    res.json("Data Dihapus")
})

app.listen(3001, ()=>{
    console.log(`http://localhost:3001`);
})
