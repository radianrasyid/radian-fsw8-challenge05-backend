# Challenge 05 Backend
Project ini merupakan bagian dari project frontend [Gitlab Project Frontend](https://gitlab.com/radianrasyid/radian-fsw8-challenge05-frontend) jadi harus dijalankan keduanya ya!

### Techs
Pada repository ini, digunakan banyak third party module seperti express, body-parser, multer, dan sequelize untuk support project ini. Penulisan html pada project ini menggunakan view engine ejs sehingga disarankan untuk menginstall ekstensi ejs snippets jika menggunakan visual studio code. Project ini merupakan sisi backend dari keseluruhan project chapter 5 dan harus dijalankan terlebih dahulu local server di dalam project ini sehingga dapat menggunakan sisi frontend dengan lebih baik.

### Database
Pada penggunaan sequelize, sudah didefinisikan database dan juga migrationnya sehingga bisa langsung dilakukan migration dengan catatan sudah menjalankan db:create dari sequelize dan database sudah dibuat dengan table dan kolom-kolom yang sama. Lalu bisa dijalankan perintah db:migrate
![ERD](https://gitlab.com/radianrasyid/radian-fsw8-challenge05-backend/-/blob/main/ERD-Radian-FSW8-Challenge5.png)

### Install all third party module
Pada saat pertama kali menjalankan project, terlebih dahulu jalankan di terminal npm install untuk menginstall semua dependencies yang ada pada package.json sehingga project bisa dijalankan dengan sempurna.

### IMPORTANT
Project ini berhubungan dengan sisi frontend sehingga harus dijalankan terlebih dahulu server di file index.js, disarankan menggunakan third party module nodemon. Setelah dijalankan nantinya jalankan server pada sisi frontend dan project bisa digunakan. API yang digunakan oleh sisi frontend dari backend ini hanyalah api get all cars dan get cars by id sehingga api untuk menjalankan proses CREATE dan UPDATE ada pada file index.js project frontend.

### RUNNING STEPS
 - Lakukan inisialisasi database di file config.js di masing-masing project dan pastikan sama.
 - Jalankan perintah sequelize db:create, ini akan membuat database yang sama dengan kolom dan tabel yang sama seperti yang ada pada bawaan project sehingga tidak perlu melakukan pembuatan model lagi.
 - Setelah itu jalankan perintah db:migrate untuk membuat tabel yang sebelumnya sudah didefinisikan pada model.
 - Jika sudah dijalankan perintah db:migrate nantinya akan ada pembuatan folder baru yang bernama migration.
 - Pastikan database sudah dibuat di dalam pgadmin atau melalui terminal
 - Jalankan project dan lakukan penambahan data dari website.
 - Website siap digunakan

